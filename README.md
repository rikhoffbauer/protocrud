# protocrud

A CLI that starts a server that makes any CRUD REST request work instantly.

## Requirements

-   nodejs
-   mongodb

## Installing

For global use run

`npm install --global protocrud`

For use in a project:

`yarn add -D protocrud` or `npm install -D protocrud`

## Usage

Run `protocrud` in your shell, a server has now started where every restful endpoint will be available.

Example for a todo model:

| Request Method | Url         | Description                 | Response               |
| -------------- | ----------- | --------------------------- | ---------------------- |
| GET            | `/todo`     | Fetches all todos           | An array of todos      |
| GET            | `/todo/:id` | Fetches a single todo by id | A single todo          |
| POST           | `/todo`     | Creates a todo              | The newly created todo |
| PUT            | `/todo/:id` | Updates a todo              | The updated todo       |
| DELETE         | `/todo/:id` | Deletes a todo              | Void                   |

> The `todo` model (as seen in the url) is arbitrary, you can use any model name you like.

## Configuration

To configure protocrud you can add a `protocrud.config.json` file next to your `package.json`,
a custom path to the config file can be provided using the --config=path/to/config.json flag.
You can override the config file by providing options to the cli, such as the --clean flag

##### protocrud.config.json

```json
{
    "port": "1234",
    "clean": false,
    "db": {
        "name": "database-name"
    }
}
```

## Initial Data

When using protocrud for a project, it can be useful to provide some initial data for your colleagues.
This can be provided by creating a `protocrud.data.json` file like so:

##### protocrud.data.json

```json
{
    "todo": [
        {
            "title": "Try protocrud",
            "completed": false
        },
        {
            "_id": "5b7deb5c18e71715587dfde7",
            "title": "Read protocrud documentation",
            "completed": true
        }
    ]
}
```

## Options

You can provide several options to the `protocrud` cli to use a different database or port for example.

All of these can also be used in the `protocrud.config.json` file,
options with dots in their names refer to deep objects, see [protocrud.config.json](#protocrudconfigjson)

| Option      | Default                   | Usage                                      | Description                                                                                |
| ----------- | ------------------------- | ------------------------------------------ | ------------------------------------------------------------------------------------------ |
| port        | `1342`                    | `protocrud --port=1342`                    | The port the protocrud server will be listening on.                                        |
| clean       | `false`                   | `protocrud --clean`                        | Removes all data when starting, imports data from the configured `data` file if it exists. |
| config      | `"protocrud.config.json"` | `protocrud --config=protocrud.config.json` | Path to the `protocrud.config.json` file.                                                  |
| prefix      | `""`                      | `protocrud --prefix=/api`                  | Path to listen on for requests, with a prefix of `/api` requests look like `GET /api/todo` |
| data        | `"protocrud.data.json"`   | `protocrud --data=protocrud.data.json`     | Path to the file with "clean" or initial data.                                             |
| db.name     | `"protocrud"`             | `protocrud --db.name=protocrud`            | The name of the mongo database that should be used to store data.                          |
| db.hostname | `"localhost"`             | `protocrud --db.hostname=localhost`        | The hostname the mongo db server is running on                                             |
| db.port     | `27017`                   | `protocrud --db.port=27017`                | The port the mongo db server is running on                                                 |
| db.user     | `undefined`               | `protocrud --db.user=username`             | The user used to connect to the mongo db server                                            |
| db.password | `undefined`               | `protocrud --db.password=username`         | The password used to connect to the mongo db server                                        |
