import { json } from "body-parser";
import express, { Request } from "express";
import { GridFSBucket, ObjectID } from "mongodb";
import multer from "multer";
import { join } from "path";
import { createReadStream } from "streamifier";
import createMongoClient from "./helpers/createMongoClient";
import getRepoAndClient from "./helpers/getRepoAndClient";
import * as templates from "./templates";
import { Config } from "./typings";

const createApp = (config: Config) => {
    const prefix = (path: string) => join(config.prefix, path);
    const app = express();
    const upload = multer({
        storage: multer.memoryStorage(),
    });

    app.post(
        prefix(config.uploadRoute),
        upload.single("file"),
        async (req, res) => {
            const client = await createMongoClient(config.db);
            const db = client.db(config.db.name);
            const bucket = new GridFSBucket(db, { bucketName: "uploads" });
            const upload$ = bucket.openUploadStream(req.file.originalname, {
                contentType: req.file.mimetype,
            });
            const id = upload$.id;

            upload$.once("finish", async () =>
                res.send(`/api/upload/${id.toString()}`),
            );
            upload$.once("error", async err =>
                res.status(500).send(err.message),
            );

            createReadStream(req.file.buffer).pipe(upload$);
        },
    );

    app.get(prefix(`${config.uploadRoute}/:id`), async (req, res) => {
        const client = await createMongoClient(config.db);
        const db = client.db(config.db.name);
        const bucket = new GridFSBucket(db, { bucketName: "uploads" });
        const download$ = bucket.openDownloadStream(
            new ObjectID(req.params.id),
        );
        download$.pipe(res);
    });

    app.get(`/protocrud/clean`, (req, res) => {
        res.send(templates.clean({ databaseName: config.db.name }));
    });

    app.post(`/protocrud/clean`, async (req, res) => {
        const client = await createMongoClient(config.db);

        await client.db(config.db.name).dropDatabase();
        await client.close();

        return res.send(
            templates.cleanSuccess({ databaseName: config.db.name }),
        );
    });

    app.use(json());

    app.get(prefix("/:model"), async (req: Request, res) => {
        const { model } = req.params;

        // ignore file requests
        if (model.match(/\.\w+$/g)) {
            return res.send(404);
        }

        try {
            const { repo, client } = await getRepoAndClient(
                config,
                req.params.model,
            );
            const result = await repo.findAll();

            await client.close();

            return res.send(result);
        } catch (err) {
            console.error(err);
            return res.status(500).send(err.message);
        }
    });

    app.get(prefix("/:model/:id"), async (req: Request, res) => {
        try {
            const { repo, client } = await getRepoAndClient(
                config,
                req.params.model,
            );
            const result = await repo.findById(req.params.id);

            await client.close();

            if (!result) {
                return res.sendStatus(404);
            }

            return res.send(result);
        } catch (err) {
            console.error(err);
            return res.status(500).send(err.message);
        }
    });

    app.post(prefix("/:model"), async (req: Request, res) => {
        try {
            const { repo, client } = await getRepoAndClient(
                config,
                req.params.model,
            );
            const result = await repo.create(req.body);
            await client.close();

            return res.status(201).send(result);
        } catch (err) {
            console.error(err);
            // TODO close client
            return res.status(500).send(err.message);
        }
    });

    app.put(prefix("/:model/:id"), async (req: Request, res) => {
        try {
            const { repo, client } = await getRepoAndClient(
                config,
                req.params.model,
            );
            const result = await repo.updateById(req.params.id, req.body);
            await client.close();

            return res.send(result);
        } catch (err) {
            console.error(err);
            return res.status(500).send(err.message);
        }
    });

    app.delete(prefix("/:model/:id"), async (req: Request, res) => {
        try {
            const { repo, client } = await getRepoAndClient(
                config,
                req.params.model,
            );
            await repo.deleteById(req.params.id);
            await client.close();

            return res.send();
        } catch (err) {
            console.error(err);
            return res.status(500).send(err.message);
        }
    });

    return app;
};

export default createApp;
