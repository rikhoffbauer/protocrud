import { Config, DatabaseConfig } from "./typings";

const validatePort = (port: number) => {
    if (typeof port !== "number" || port === NaN) {
        throw new Error(`Port is not a number.`);
    }

    if (port < 1024) {
        throw new Error(
            `Port not in range, ${port} is too small; min port number is 1024.`,
        );
    }
    if (port > 49151) {
        throw new Error(
            `Port not in range, ${port} is too large; max port number is 49151.`,
        );
    }
};

const validateConfig = ({ port, db }: Config) => {
    validatePort(port);
    validateDbConfig(db);
};

const validateDbConfig = ({ port }: DatabaseConfig) => {
    validatePort(port);
};

export default validateConfig;
