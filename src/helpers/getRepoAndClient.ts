import { pathExists } from "fs-extra";
import { resolve } from "path";
import { Config } from "../typings";
import createMongoClient from "./createMongoClient";
import createRepository from "./createRepository";

const getRepoAndClient = async (config: Config, model: string) => {
    const client = await createMongoClient(config.db);
    const dataPath = resolve(process.cwd(), `protocrud.data.json`);
    let initialData;

    if (await pathExists(dataPath)) {
        delete require.cache[dataPath];
        initialData = require(dataPath)[model];
    }

    const repo = createRepository({
        db: client.db(config.db.name),
        model: model,
        initialData,
    });

    return { repo, client };
};

export default getRepoAndClient;
