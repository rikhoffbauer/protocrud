import { Collection, Db, ObjectID } from "mongodb";
import { Model, Omit } from "../typings";

export interface RepositoryOptions<T extends Model> {
    db: Db;
    model: string;
    initialData?: T[];
}

export interface Repository<T extends Model> {
    create(model: Omit<T, "_id">): Promise<T>;
    deleteById(id: string | ObjectID): Promise<void>;
    findAll(): Promise<T[]>;
    findById(id: string | ObjectID): Promise<T>;
    updateById(
        id: string | ObjectID,
        model: Partial<Omit<T, "_id">>,
    ): Promise<T>;
}

const idQuery = (id: string | ObjectID) => ({
    _id: { $eq: ensureObjectId(id) },
});

const ensureObjectId = (id: string | ObjectID) =>
    typeof id === "string" ? new ObjectID(id) : id;

const createRepository = <T extends Model>({
    db,
    model,
    initialData,
}: RepositoryOptions<T>) => {
    const insertInitialData = async (coll: Collection) => {
        if (initialData && initialData.length) {
            await coll.insertMany(
                initialData.map(model => ({
                    ...(model as {}),
                    _id: ensureObjectId(model._id),
                })),
            );
        }
    };
    const ensureCollection: () => Promise<Collection> = async () => {
        const collections = await db.listCollections().toArray();
        const collectionNames = collections.map(coll => coll.name);

        if (collectionNames.includes(model)) {
            return db.collection(model);
        }

        const collection = await db.createCollection(model);

        await insertInitialData(collection);

        return collection;
    };

    const repository: Repository<T> = {
        create: async model => {
            const collection = await ensureCollection();
            const result = await collection.insertOne(model);

            return result.ops[0];
        },
        deleteById: async id => {
            const collection = await ensureCollection();
            // TODO dont we need idQuery?
            await collection.deleteOne({ _id: id });

            return;
        },
        findAll: async () => {
            const collection = await ensureCollection();
            return collection.find().toArray();
        },
        findById: async id => {
            const collection = await ensureCollection();
            return await collection.findOne(idQuery(id));
        },
        updateById: async (id, model) => {
            const collection = await ensureCollection();
            const result = await collection.findOneAndUpdate(
                idQuery(id),
                { $set: { ...(model as {}), _id: ensureObjectId(id) } },
                { upsert: true, returnOriginal: false },
            );

            return result.value;
        },
    };

    return repository;
};

export default createRepository;
