import { MongoClient } from "mongodb";
import { DatabaseConfig } from "../typings";
import createMongoUrl from "./createMongoUrl";

const createMongoClient = async (config: DatabaseConfig) => {
    return await MongoClient.connect(
        createMongoUrl(config),
        { useNewUrlParser: true },
    );
};

export default createMongoClient;
