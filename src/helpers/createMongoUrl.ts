import { DatabaseConfig } from "../typings";

const createMongoUrl = ({
    user,
    password,
    port,
    hostname,
    name,
}: DatabaseConfig) => {
    const auth = password ? `${user}:${password}@` : ``;
    return `mongodb://${auth}${hostname}:${port}/${name}`;
};

export default createMongoUrl;
