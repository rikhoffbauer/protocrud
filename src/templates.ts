interface CleanProps {
    databaseName: string;
}

export const clean = ({ databaseName }: CleanProps) =>
    `
<html>
    <body>
        <h3>Clean database "${databaseName}"</h3>
        <form method="post">
            <button type="submit">Clean!</button>
        </form>
    </body>
</html>`;

interface CleanSuccessProps {
    databaseName: string;
}

export const cleanSuccess = ({ databaseName }: CleanSuccessProps) =>
    `
<html>
    <body>
        <h3>Database "${databaseName}" cleaned successfully!</h3>
        <form method="post">
            <button type="submit">Clean again!</button>
        </form>
    </body>
</html>`;
