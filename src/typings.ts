export interface Config {
    uploadRoute: string;
    port: number;
    prefix: string;
    clean: boolean;
    config: string;
    data: string;
    db: DatabaseConfig;
}

export interface DatabaseConfig {
    hostname: string;
    port: number;
    name: string;
    user?: string;
    password?: string;
}

export type Diff<
    T extends string | number | symbol,
    U extends string | number | symbol
> = ({ [P in T]: P } & { [P in U]: never } & { [x: string]: never })[T];

export type Omit<T, K extends keyof T> = Pick<T, Diff<keyof T, K>>;

export interface Model {
    _id: string;
}
