#!/usr/bin/env node

import { pathExists } from "fs-extra";
import { MongoClient } from "mongodb";
import { resolve } from "path";
import { argv } from "yargs";
import createApp from "../app";
import createMongoUrl from "../helpers/createMongoUrl";
import { Config } from "../typings";
import validateConfig from "../validateConfig";
import defaults = require("lodash.defaultsdeep");
const { version } = require("../../package.json");

export interface Argv {
    port?: number | string;
    prefix?: string;
    clean?: boolean;
    config?: string;
    data?: string;
    "db.name"?: string;
    "db.hostname"?: string;
    "db.user"?: string;
    "db.password"?: string;
    "db.port"?: number | string;
}

const extractConfigFromArguments = async (argv: Argv): Promise<Config> => {
    const defaultConfig: Config = {
        port: 1342,
        uploadRoute: "/upload",
        prefix: "",
        clean: false,
        config: "protocrud.config.json",
        data: "protocrud.data.json",
        db: {
            hostname: "localhost",
            port: 27017,
            name: "protocrud",
        },
    };
    let config = {
        port: argv.port,
        prefix: argv.prefix,
        clean: argv.clean,
        config: argv.config,
        data: argv.data,
        db: {
            hostname: argv["db.hostname"],
            port: Number(argv["db.port"]),
            name: argv["db.name"],
            user: argv["db.user"],
            password: argv["db.password"],
        },
    } as Config;

    const configPath = resolve(
        process.cwd(),
        config.config || defaultConfig.config,
    );

    if (await pathExists(configPath)) {
        delete require.cache[configPath];
        const configFromFile: Partial<Config> = require(configPath);

        config = defaults(config, configFromFile, defaultConfig);

        config = {
            ...defaults(config, configFromFile),
            db: {
                ...defaults(config.db, configFromFile.db),
            },
        };

        config = {
            ...config,
            port: Number(config.port),
            db: {
                ...config.db,
                port: Number(config.db.port),
            },
        };
    }

    validateConfig(config);

    return config;
};

const start = async () => {
    const config = await extractConfigFromArguments(argv as Argv);
    const app = createApp(config);

    if (config.clean) {
        const client = await MongoClient.connect(
            createMongoUrl(config.db),
            { useNewUrlParser: true },
        );

        await client.db(config.db.name).dropDatabase();
    }

    app.listen(config.port, () => {
        try {
            validateConfig(config);
        } catch (err) {}

        console.log(`protocrud ${version} listening on port ${config.port}`);
    });
};

start().catch(err => {
    console.error(`Failed to start protocrud: ${err.message}`);
    process.exit(1);
});
